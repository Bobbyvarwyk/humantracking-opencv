from threading import Thread
import cv2

class StreamCapture:
    """
    Class which opens the video stream in cv2 VideoCapture
    """

    def __init__(self, src=0):
        self.stream = cv2.VideoCapture(src)
        (self.grabbed, self.frame) = self.stream.read()
        self.stopped = False

    def start(self):
        Thread(target=self.getStream, args=()).start()
        return self
    
    def getStream(self):
        while not self.stopped:
            if not self.grabbed:
                self.stop()
            else:
                (self.grabbed, self.frame) = self.stream.read()
    
    def stop(self):
        self.stopped = True
