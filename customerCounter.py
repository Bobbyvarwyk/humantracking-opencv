### IMPORT THE PACKAGES
from pyimagesearch.centroidtracker import CentroidTracker
from pyimagesearch.trackableobject import TrackableObject
from imutils.video import VideoStream
from imutils.video import FPS
from StreamCapture import StreamCapture
import numpy as np
import argparse
import imutils
import time
import dlib
import cv2

# Define the argument parsers for run via command line
ap = argparse.ArgumentParser()
ap.add_argument("-p", "--prototxt", help="Path to Caffe deploy prototxt file", default="mobilenet_ssd/MobileNetSSD_deploy.prototxt") 
ap.add_argument("-m", "--model", help="path to Caffe pre-trained model", default="mobilenet_ssd/MobileNetSSD_deploy.caffemodel")
ap.add_argument("-i", "--input", type=str, help="path to input stream")
ap.add_argument("-c", "--confidence", type=float, default=0.4, help="minimum propability to filter weak detections")
ap.add_argument("-s", "--skip-frames", type=int, default=30, help="# of skip frames between detections")
args = vars(ap.parse_args())


CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
	"bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
	"dog", "horse", "motorbike", "person", "pottedplant", "sheep",
	"sofa", "train", "tvmonitor"]

# Load model from disk
net = cv2.dnn.readNetFromCaffe(args["prototxt"], args["model"])

# Initiate centroid tracker, trackers list and trackable objects, also totalFrames
ct = CentroidTracker(maxDisappeared=40, maxDistance=50)
trackers = []
trackableObjects = {}
totalFrames = 0

# Initiate the frame dimensions
W = None
H = None

vStream = StreamCapture(args["input"]).start()

while True:
    # Read the frames from the videostream
    frame = vStream.frame

    # Resize the frame to a maximum of width of 500 pixels to reduce processing time
    frame = imutils.resize(frame, width=500)
    # Convert the frame to RGB color for dlib
    rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)    

    # Set the frame dimensions
    if W is None or H is None:
        (H, W) = frame.shape[:2]

    rects = []

    # Detect all humans in the videostream
    if totalFrames % args["skip_frames"] == 0:
        trackers = []

        # Convert the frame to a Blob to obtain detections
        frameBlob = cv2.dnn.blobFromImage(frame, 0.007843, (W, H), 127.5)
        net.setInput(frameBlob)
        detections = net.forward()

        # Loop over the detections made
        for i in np.arange(0, detections.shape[2]):
            # Extract the confidence of the detection
            confidence = detections[0, 0, i, 2]

            # Filter out the weak detections by comparing it to our minimum confidence
            if confidence > args["confidence"]:
                # Extract the index of the class label from the detections
                indexLabel = int(detections[0, 0, i, 1])

                if CLASSES[indexLabel] != "person":
                    continue
                
                # Compute coordinates (X, Y) for the bounding box of the object
                box = detections[0, 0, i, 3:7] * np.array([W, H, W, H])
                (startX, startY, endX, endY) = box.astype("int")

                # Construct a dlib object from the bounding box coordinates and start a dlib correlation tracker
                tracker = dlib.correlation_tracker()
                rect = dlib.rectangle(startX, startY, endX, endY)
                tracker.start_track(rgb, rect)

                # Add the tracker to our trackers list
                trackers.append(tracker)

    else:
        for tracker in trackers:
            tracker.update(rgb)
            pos = tracker.get_position()

            startX = int(pos.left())
            startY = int(pos.top())
            endX = int(pos.right())
            endY = int(pos.bottom())

            rects.append((startX, startY, endX, endY))

    # Use the centroid tracker to associate the old object centroid with the new object
    objects = ct.update(rects)

    # Loop through tracked objects
    for (objectID, centroid) in objects.items():
        # Check if a trackable object already exists for the current one
        to =  trackableObjects.get(objectID, None)

        # If there is no existing object, create it
        if to is None:
            to = TrackableObject(objectID, centroid)

        trackableObjects[objectID] = to
        
        text = "ID {}".format(objectID)
        cv2.putText(frame, text, (centroid[0] - 10, centroid[1] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

    cv2.imshow("Frame", frame)
    key = cv2.waitKey(1) & 0xFF

    if key == ord("q"):
        vStream.stop()
        break

    totalFrames += 1

cv2.destroyAllWindows()